/**
 * 
 */
package com.loanLedger.model;

import com.orm.SugarApp;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.Date;


/**
 * @author atif
 *
 */

//@Entity
//@Table(name = "T_Reminder")
public class Reminder extends SugarRecord implements Serializable {

	/**
	 * 
	 */


//	@Type(type="yes_no")
	private Boolean autoReminder=false;
	
//	@Column(name = "date")
//	@Type(type="date")
	private Date date;
	
//	@Enumerated(EnumType.STRING)
//	@Column(name = "type")
	private ReminderType type=ReminderType.SMS;

	
	public Reminder() {
	}

	public Reminder(Boolean autoReminder, Date date, ReminderType type) {
		super();
		this.autoReminder = autoReminder;
		this.date = date;
		this.type = type;
	}

	public Boolean getAutoReminder() {
		return autoReminder;
	}

	public void setAutoReminder(Boolean autoReminder) {
		this.autoReminder = autoReminder;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public ReminderType getType() {
		return type;
	}

	public void setType(ReminderType type) {
		this.type = type;
	}

	
}
