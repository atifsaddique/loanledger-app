/**
 * 
 */
package com.loanLedger.model;

import com.orm.SugarApp;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.Date;


/**
 * @author atif
 *
 */

//@Entity
//@Table(name = "T_Items")
public class Item extends SugarRecord implements Serializable {
     
	/**
	 * 
	 */
//
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	//private Long id;

//	@Column(name = "title")
	private String title;
	
//	@Column(name = "description")
	private String description;
	
//	@Enumerated(EnumType.STRING)
//	@Column(name = "type")
	private ItemType type=ItemType.Cash;
	
//	@Column(name = "loanDate")
//	@Type(type="date")
	private Date loanDate;
	
//	@OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.DETACH)
//	@JoinColumn(name="reminder_id")
	private Reminder reminder;
//
//	@OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.DETACH)
//	@JoinColumn(name="borrower_id")
	private Borrower borrower;
	
//	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.DETACH)
//    @JoinColumn(name="id_user")
	private User user;
	
	public Item() {
		
	}

	public Item(String title, String description, ItemType type, Date loanDate) {
		super();
		this.title = title;
		this.description = description;
		this.type = type;
		this.loanDate = loanDate;
	}

	
	
	public Date getLoanDate() {
		return loanDate;
	}

	public void setLoanDate(Date loanDate) {
		this.loanDate = loanDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the itemType
	 */
	public ItemType getType() {
		return type;
	}

	/**
	 * @param itemType the itemType to set
	 */
	public void setType(ItemType type) {
		this.type = type;
	}

	public Reminder getReminder() {
		return reminder;
	}

	public void setReminder(Reminder reminder) {
		this.reminder = reminder;
	}

	public Borrower getBorrower() {
		return borrower;
	}

	public void setBorrower(Borrower borrower) {
		this.borrower = borrower;
	}
}
