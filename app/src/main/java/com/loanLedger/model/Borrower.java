/**
 * 
 */
package com.loanLedger.model;

import com.orm.SugarApp;
import com.orm.SugarRecord;

import java.io.Serializable;


/**
 * @author atif
 *
 */

//@Entity
//@Table(name = "T_Borrower")
public class Borrower extends SugarRecord implements Serializable {

//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	//private Long id;

//	@Column(name = "name")
	private String name;

//	@Column(name = "email")
	private String email;
	
//	@Column(name = "phoneNumber")
	private String phoneNumber;
	
	public Borrower() {
	}

	public Borrower(String name, String email, String phoneNumber) {
		super();
		this.name = name;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
}
