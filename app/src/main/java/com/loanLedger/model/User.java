/**
 * 
 */
package com.loanLedger.model;

import com.orm.SugarApp;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


//@Entity
//@Table(name = "T_Users")
public class User extends SugarRecord implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5728100783405199628L;

//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

//	@Column(name = "name")
	private String name;
//
//	@Column(name = "email")
	private String email;
	
//	@Column(name = "phoneNumber")
	private String phoneNumber;
	
//	@Column(name = "password")
	private String password;
	
//	@OneToMany(mappedBy="user", fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	List<Item> items=new ArrayList<Item>();

	public User(){
	}
	
	public User(String name, String email, String phoneNumber, String password) {
		super();
		this.name = name;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.password = password;
	}




	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	@Override
	public String toString(){
		return "{username : "+name+"  password:"+password+"}";	
	}
}
