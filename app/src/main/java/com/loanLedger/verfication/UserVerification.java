/**
 * 
 */
package com.loanLedger.verfication;

import com.orm.SugarApp;
import com.orm.SugarRecord;

/**
 * @author atif
 *
 */

//@Entity
//@Table(name = "T_UserVerification")
public class UserVerification extends SugarRecord {
	
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

//	@Column(name = "email")
	private String email;
	
//	@Column(name = "code")
	private String code;

	public UserVerification() {
		
	}

	public UserVerification(String email, String code) {
		super();
		this.email = email;
		this.code = code;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
