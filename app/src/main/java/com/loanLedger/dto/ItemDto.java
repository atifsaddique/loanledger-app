package com.loanLedger.dto;

import com.loanLedger.model.Item;

import java.io.Serializable;

/**
 * Created by abdulaziz on 12/24/15.
 */
public class ItemDto implements Serializable {
    private  String email;
    private Item item;

    public ItemDto() {

    }

    public ItemDto(String email, Item item) {
        super();
        this.email = email;
        this.item = item;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

}