package aurora.hackathon.app.list;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hazim on 12/23/15.
 */
public class ListItemDTO {
    private String itemTitle;
    private String borrowerName;
    private Date loanDate;
    private Date returnDate;

    private static DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    public ListItemDTO(String itemTitle, String borrowerName, Date loanDate, Date returnDate){
        this.itemTitle = itemTitle;
        this.borrowerName = borrowerName;
        this.loanDate = loanDate;
        this.returnDate = returnDate;
    }
    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getBorrowerName() {
        return borrowerName;
    }

    public void setBorrowerName(String borrowerName) {
        this.borrowerName = borrowerName;
    }

    public Date getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(Date loanDate) {
        this.loanDate = loanDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public String getLoanDateOnly(){
            return formatter.format(loanDate);
    }

    public String getReturnDateOnly(){
        return formatter.format(returnDate);
    }
}
