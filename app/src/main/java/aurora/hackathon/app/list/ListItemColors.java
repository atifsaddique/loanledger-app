package aurora.hackathon.app.list;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by hazim on 12/24/15.
 */
public enum ListItemColors {
    RED("#DD2C00"),
    BLUE("#00B8D4"),
    YELLOW("#FFC107"),
    GREEN("#00BFA5");

    private String value;

    private ListItemColors(String value){
        this.value=value;
    }

    @Override
    public String toString(){
        return value;
    }

    private static final List<ListItemColors> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static ListItemColors randomColor()  {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }
}
