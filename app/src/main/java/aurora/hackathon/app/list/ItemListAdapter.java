package aurora.hackathon.app.list;

/**
 * Created by hazim on 12/23/15.
 */

import android.app.LauncherActivity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import aurora.hackathon.app.LoanLedger;
import aurora.hackathon.app.R;

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ViewHolder> {
    private List<ListItemDTO> mDataset;

    // View holder to access all view items in the layout
    public class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout rlItemRow;
        public TextView tvItemTitle;
        public TextView tvBorrowerName;
        public TextView tvLoanDate;
        public TextView tvReturnDate;
        public LinearLayout llItemIcon;

        public ViewHolder(View v) {
            super(v);
            rlItemRow = (RelativeLayout) v.findViewById(R.id.item_row);
            tvItemTitle = (TextView) v.findViewById(R.id.item_title);
            tvBorrowerName = (TextView) v.findViewById(R.id.borrower_name);
            tvLoanDate = (TextView) v.findViewById(R.id.loan_date);
            tvReturnDate = (TextView) v.findViewById(R.id.return_date);
            llItemIcon = (LinearLayout) v.findViewById(R.id.icon);
        }
    }

    public void add(int position, ListItemDTO item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(ListItemDTO item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    // Constructor with dataset
    public ItemListAdapter(List<ListItemDTO> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ItemListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ListItemDTO listItem = mDataset.get(position);
        holder.tvItemTitle.setText(listItem.getItemTitle());
        holder.rlItemRow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Display Item details", Toast.LENGTH_LONG).show();
                //remove(listItem);
            }
        });

        holder.tvBorrowerName.setText("Borrowed by: " + listItem.getBorrowerName());
        holder.tvLoanDate.setText(listItem.getLoanDateOnly());
        holder.tvReturnDate.setText(listItem.getReturnDateOnly());
        holder.llItemIcon.setBackgroundColor(Color.parseColor(ListItemColors.randomColor().toString()));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}