package aurora.hackathon.app;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.loanLedger.model.Borrower;
import com.loanLedger.model.Item;
import com.loanLedger.model.ItemType;
import com.loanLedger.model.Reminder;
import com.loanLedger.model.ReminderType;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import aurora.hackathon.app.reminders.ReminderActivator;
import aurora.hackathon.app.rest.LoanRestClient;


public class AddItemActivity extends AppCompatActivity {

    DateFormat fmtDateAndTime=DateFormat.getDateTimeInstance();
    TextView dateAndTimeLabel;
    Calendar dateAndTimeSchedule=Calendar.getInstance();

    private static final int RESULT_PICK_CONTACT = 100;
    private TextView textViewName;
    private TextView textViewNumber;
    private TextView textViewEmail;

    private TextView tvItemTitle, tvItemDescription;
    private RadioGroup rgItemType, rgReminderType;
    private RadioButton rbCash, rbStuff, rbSms, rbEmail;

    private Switch autoReminderSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton btnSchedule=(FloatingActionButton)findViewById(R.id.fabCalendar);
        Button btnPickContact = (Button)findViewById(R.id.button1);

        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewNumber = (TextView) findViewById(R.id.textViewNumber);
        textViewEmail = (TextView) findViewById(R.id.textViewEmail);

        tvItemTitle = (TextView) findViewById(R.id.txtTitle);
        tvItemDescription = (TextView) findViewById(R.id.txtDescription);

        rgItemType = (RadioGroup) findViewById(R.id.radioBorrowType);
        rbCash = (RadioButton) findViewById(R.id.radioButtonCash);
        rbStuff = (RadioButton) findViewById(R.id.radioButtonStuff);

        rgReminderType = (RadioGroup) findViewById(R.id.radioReminderType);
        rbSms = (RadioButton) findViewById(R.id.radioButtonSMS);
        rbEmail = (RadioButton) findViewById(R.id.radioButtonEmail);

        autoReminderSwitch = (Switch) findViewById(R.id.switchAutoReminder);

        dateAndTimeLabel=(TextView)findViewById(R.id.dateAndTimeSchedule);


        //set sms permissions true
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);
        //set read-contact permissions true
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, 1);
        //set get-accounts permissions true
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_ACCOUNTS}, 1);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(dateAndTimeLabel.getText().toString() == null || dateAndTimeLabel.getText().toString().isEmpty() ){
                    return;
                }
                Borrower browwer = new Borrower(textViewName.getText().toString(), textViewEmail.getText().toString(),textViewNumber.getText().toString() );
                Reminder reminder = null;
                try {
                    reminder = new Reminder(true, new SimpleDateFormat("dd MMM yyyy hh:mm:ss").parse(dateAndTimeLabel.getText().toString()), ReminderType.SMS);
                } catch (ParseException e) {
                    try {
                        reminder = new Reminder(true, new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aa").parse(dateAndTimeLabel.getText().toString()), ReminderType.SMS);
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                    e.printStackTrace();
                }

                Item item = new Item(tvItemTitle.getText().toString(), tvItemDescription.getText().toString(), ItemType.Stuff, new Date());
                item.setReminder(reminder);
                item.setBorrower(browwer);
                browwer.save();
                reminder.save();
                item.save();

                //call to server to save items on server too.
               // LoanRestClient.postSaveItem(AddItemActivity.this, inItem, "hazim@aurorasolutions.io");
                Toast.makeText(AddItemActivity.this, item.getTitle()+" "+item.getType()+" "+item.getBorrower().getName(), Toast.LENGTH_LONG).show();
                String smsNumberToSend = browwer.getPhoneNumber();
                String smsTextToSend = "Please return my "+item.getTitle();
                ReminderActivator.activateReminderSMS(smsNumberToSend, smsTextToSend, AddItemActivity.this, reminder.getDate());
            }
        });


        btnSchedule.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new DatePickerDialog(AddItemActivity.this,
                        d,
                        dateAndTimeSchedule.get(Calendar.YEAR),
                        dateAndTimeSchedule.get(Calendar.MONTH),
                        dateAndTimeSchedule.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btnSchedule=(FloatingActionButton)findViewById(R.id.fabTime);

        btnSchedule.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new TimePickerDialog(AddItemActivity.this,
                        t,
                        dateAndTimeSchedule.get(Calendar.HOUR_OF_DAY),
                        dateAndTimeSchedule.get(Calendar.MINUTE),
                        true).show();
            }
        });

        btnPickContact.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                pickContact(v);
            }
        });


        //updateLabelSchedule();
    }
    public void pickContact(View v)
    {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check whether the result is ok
        if (resultCode == RESULT_OK) {
            // Check for the request code, we might be usign multiple startActivityForReslut
            switch (requestCode) {
                case RESULT_PICK_CONTACT:
                    contactPicked(data);
                    break;
            }
        } else {
            Log.e("MainActivity", "Failed to pick contact");
        }
    }

    /**
     * Query the Uri and read contact details. Handle the picked contact data.
     * @param data
     */
    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null ;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            // Set the value to the textviews
            textViewName.setText(name);
            textViewNumber.setText(phoneNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            dateAndTimeSchedule.set(Calendar.YEAR, year);
            dateAndTimeSchedule.set(Calendar.MONTH, monthOfYear);
            dateAndTimeSchedule.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabelSchedule();
        }
    };
    TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay,
                              int minute) {
            dateAndTimeSchedule.set(Calendar.HOUR_OF_DAY, hourOfDay);
            dateAndTimeSchedule.set(Calendar.MINUTE, minute);
            updateLabelSchedule();
        }
    };

    private void updateLabelSchedule() {
        dateAndTimeLabel.setText(fmtDateAndTime.format(dateAndTimeSchedule.getTime()));
    }


};

