package aurora.hackathon.app.reminders;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by hazim on 12/24/15.
 */
public class ReminderActivator {
    private static PendingIntent pendingIntent;

    public static void activateReminderSMS(String smsNumber, String smsText, Activity activity, Date returnDate){
        smsText +="\n -------- \n Automatic Reminder sent by \"Loan Ledger\"";

        Intent myIntent = new Intent(activity, AlarmSmsService.class);

        Bundle bundle = new Bundle();
        bundle.putCharSequence("extraSmsNumber", smsNumber);
        bundle.putCharSequence("extraSmsText", smsText);
        myIntent.putExtras(bundle);

        pendingIntent = PendingIntent.getService(activity, 0, myIntent, 0);

        AlarmManager alarmManager = (AlarmManager) activity.getSystemService(activity.ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        Date currentDate = new Date();
        long seconds = (returnDate.getTime()-currentDate.getTime())/1000;
        calendar.add(Calendar.SECOND, (int)seconds);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }
}
