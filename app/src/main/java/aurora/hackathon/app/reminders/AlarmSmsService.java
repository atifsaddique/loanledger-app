package aurora.hackathon.app.reminders;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.widget.Toast;
/**
 * Created by hazim on 12/24/15.
 */
public class AlarmSmsService extends Service {

    String smsNumberToSend, smsTextToSend;

    @Override
    public void onCreate() {

    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sendSms(intent, startId);
        return START_NOT_STICKY;
    }

    public void sendSms(Intent intent, int startId){
        Bundle bundle = intent.getExtras();
        smsNumberToSend = (String) bundle.getCharSequence("extraSmsNumber");
        smsTextToSend = (String) bundle.getCharSequence("extraSmsText");

        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(smsNumberToSend, null, smsTextToSend, null, null);
        Toast.makeText(this, "SMS reminder sent to user", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

}