package aurora.hackathon.app;

import android.content.Intent;
import android.widget.Toast;

import com.orm.SugarApp;

/**
 * Created by hazim on 12/24/15.
 */
public class AppStart extends SugarApp {

    @Override
    public void onCreate(){
        super.onCreate();
        Intent intent = new Intent(AppStart.this, LoanLedger.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        AppStart.this.startActivity(intent);
    }
}
