package aurora.hackathon.app;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.loanLedger.model.Item;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import aurora.hackathon.app.list.ItemListAdapter;
import aurora.hackathon.app.list.ListItemDTO;

public class LoanLedger extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_ledger);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.item_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Iterator<Item> items = Item.findAll(Item.class);
        List<ListItemDTO> listItemDTOs = new ArrayList<>();

        while(items.hasNext()){
            Item item = items.next();
            listItemDTOs.add(new ListItemDTO(item.getTitle(), item.getBorrower().getName(), item.getLoanDate(), item.getReminder().getDate()));
        }
        listItemDTOs.add(new ListItemDTO("macbook", "atif", new Date(), new Date()));

        mAdapter = new ItemListAdapter(listItemDTOs);
        mRecyclerView.setAdapter(mAdapter);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent myIntent = new Intent(LoanLedger.this, AddItemActivity.class);
                LoanLedger.this.startActivity(myIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_load_ledger, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Iterator<Item> items = Item.findAll(Item.class);
        List<ListItemDTO> listItemDTOs = new ArrayList<>();

        while(items.hasNext()){
            Item item = items.next();
            listItemDTOs.add(new ListItemDTO(item.getTitle(), item.getBorrower().getName(), item.getLoanDate(), item.getReminder().getDate()));
        }
        listItemDTOs.add(new ListItemDTO("macbook", "atif", new Date(), new Date()));

        mAdapter = new ItemListAdapter(listItemDTOs);
        mRecyclerView.setAdapter(mAdapter);

        int id = menuItem.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(menuItem);
    }
}
