# Loan Ledger

## Quickstart

From the command line do:

git clone https://bitbucket.org/atifsaddique/loanledger-app.git
cd loanledger-app


## IDE setup

For setting up the development environment in an IDE do the following:

1. Download and install Java 8 JDK 
2. Make sure you have an Android Studio
3. Make sure you have the latest sdk tools